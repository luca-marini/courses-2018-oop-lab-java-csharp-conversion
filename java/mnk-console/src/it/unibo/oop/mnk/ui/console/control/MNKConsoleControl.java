
package it.unibo.oop.mnk.ui.console.control;

import it.unibo.oop.mnk.MNKMatch;

public interface MNKConsoleControl {
	MNKMatch getModel();
    void setModel(MNKMatch model);
    void input();
    void makeMove(int i, int j);

    static MNKConsoleControl of() {
        return new MNKConsoleConsoleControlImpl();
    }
    static MNKConsoleControl of(MNKMatch match) {
        final MNKConsoleControl control = new MNKConsoleConsoleControlImpl();
        control.setModel(match);
        return control;
    }
}
