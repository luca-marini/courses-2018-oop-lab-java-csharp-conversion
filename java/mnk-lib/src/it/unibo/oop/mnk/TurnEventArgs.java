package it.unibo.oop.mnk;

import java.util.Objects;

public class TurnEventArgs extends MNKEventArgs {
    public TurnEventArgs(MNKMatch source, int turn, Symbols player, int i, int j) {
        super(source, turn, Objects.requireNonNull(player), i, j);
    }
}
