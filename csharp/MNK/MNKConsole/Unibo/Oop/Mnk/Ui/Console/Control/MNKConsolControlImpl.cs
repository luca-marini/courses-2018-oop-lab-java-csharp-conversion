using System;
using System.IO;

namespace Unibo.Oop.Mnk.Ui.Console.Control
{
    internal class MNKConsolControlImpl : IMNKConsoleControl
    {
        private readonly StringReader reader = new StringReader(null);
        private IMNKMatch model;

        public IMNKMatch Model
        {
            get { return this.model; }
            set { this.model = value; }
        }

        public void Input()
        {
            try
            {
                if (model != null)
                {
                    while (true)
                    {
                        string line = reader.ReadLine();
                        string[] coords = line.Split(' ', '+');

                        if (line.Equals("reset"))
                        {
                            model.Reset();
                            break;
                        }
                        else if (line.Equals("exit"))
                        {
                            System.Environment.Exit(0);
                        }
                        else if (coords.Length.Equals(2))
                        {
                            string fst = coords[0];
                            string snd = coords[1];
                            int i = fst[0] - 'a';

                            try
                            {
                                int j = int.Parse(snd) - 1;
                                MakeMove(i, j);
                                break;
                            }
                            catch (Exception e)
                            {
                                ;
                            }
                        }
                        else
                        {
                            System.err;
                        }
                    }
                }

            }
            catch (IOException e) {
                Console.WriteLine(StackTrace.ToString());
            }
           
        }

        public void MakeMove(int i, int j)
        {
            if (model != null)
            {
                model.Move(i, j);
            }
        }
    }
}