﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    class OrderedEventSource<TArg> : AbstractEventSource<TArg>
    {
        private IList<EventListener<TArg>> eventListeners = new List<EventListener<TArg>>();
        protected override Collection<EventListener<TArg>> EventListeners()
        {
            return new Collection<EventListener<TArg>>(eventListeners);
        }
    }
}
