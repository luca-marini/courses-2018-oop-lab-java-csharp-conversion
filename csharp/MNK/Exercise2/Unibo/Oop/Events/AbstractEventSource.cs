﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    abstract class AbstractEventSource<TArg> : IEventSource<TArg>, IEventEmitter<TArg>
    {

        protected abstract Collection<EventListener<TArg>> EventListeners();

        public IEventSource<TArg> EventSource => this;

        public void Bind(EventListener<TArg> eventListener)
        {
            EventListeners().Add(eventListener);
        }

        public void Emit(TArg data)
        {
            foreach (EventListener < TArg > e in EventListeners())
            {
                e.Invoke(data);
            }  
        }

        public void Unbind(EventListener<TArg> eventListener)
        {
            EventListeners().Remove(eventListener);
        }

        public void UnbindAll()
        {
            EventListeners().Clear();
        }
    }
}
