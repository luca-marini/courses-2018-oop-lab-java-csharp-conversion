using System;

namespace Unibo.Oop.Mnk
{
    public class MatchEventArgs : MNKEventArgs
    {
        public MatchEventArgs(IMNKMatch source, int turn, Symbols player, Symbols? winner, int i, int j) 
            : base(source, turn, player, i, j)
        {
            this.Winner = winner;
        }

        public Symbols? Winner { get; }
    }
}