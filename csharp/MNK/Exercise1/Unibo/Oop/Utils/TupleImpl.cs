﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        private readonly Object[] items;

        public TupleImpl(object[] args)
        {
            this.items = args;
        }

        public object this[int i]
        {
            get { return items[i]; }
        }

        public int Length
        {
            get { return this.items.Length; }
        }

        public object[] ToArray()
        {
            return this.items.ToArray();
        }

        public override String ToString()
        {
            //return Array.ForEach(items,ToString().Join(",", items.ToString()));
            return "(" + string.Join(", ", items) + ")";
        }

        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;

            foreach(Object obj in items)
            {
                result = prime * result + obj.GetHashCode();
            }
            return result;
        }

        public override bool Equals(Object obj)
        {
            if (obj != null && obj is TupleImpl)
            {

                return items.SequenceEqual(((TupleImpl)obj).items);
            }
            return false;
        }
    }
}
